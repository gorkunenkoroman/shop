<?php
namespace App\Http\Controllers\Admin;
use Image;
use App\Products;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class PostsController extends Controller
{
    public function index() {
        $all = Products::where('user_id', Auth::user()->id)->paginate(10);
        return view('admin.posts.index', ['all'=>$all]);
    }

    public function nine() {
        $all = Products::paginate(9);
        return view('welcome', ['all'=>$all]);     
    }
    
    public function admin() {
        $all = Products::paginate(9);
        return view('admin.dashboard', ['all'=>$all]);     
    }
    
    public function show($id)
    {
        $post = Products::where('id', $id)->firstOrFail();
        return view('product', compact('post'));
    }
    
    public function create(Request $request) {   
        $products= new Products();    
        if($request->method()=='POST'){
            $products->title = strip_tags(trim($request->title));
            $products->short_description = strip_tags(trim($request->short_description));
            $products->description = strip_tags(trim($request->description));
            if (!empty($request->file('image'))){
                foreach( $request->file('image') as $image ){
                //$image = $request->file('image');
                $upload = './uploads';
                $filename = $image->getClientOriginalName();
                $img = Image::make($image)->resize(1024, 768);
                $img->save($upload.'/'.$filename);
                $products->filename = strip_tags(trim($filename));
                $images[]=$products->filename; 
                $products->filename= implode(",",$images);
                }
            }
            $products->user_id = Auth::user()->id;
            $products->price = strip_tags(trim($request->price));
            $products->save();
            return redirect()->route('products');
        }
        return view('admin.posts.create');
    }
    
    public function edit($id, Request $request) {
        $productsClass = Products::find($id);  
        if($request->method()=='POST'){
            $productsClass->title = strip_tags(trim($request->title1));
            $productsClass->short_description = strip_tags(trim($request->short_description1));
            $productsClass->description = strip_tags(trim($request->description1));
            if (!empty($request->file('images'))){
                foreach( $request->file('images') as $image1 ){
                //$image1 = $request->file('images');
                $upload1 = './uploads';
                $filename1 = $image1->getClientOriginalName();
                $img1 = Image::make($image1)->resize(1024, 768);
                $img1->save($upload1.'/'.$filename1);   
                $productsClass->filename = strip_tags(trim($filename1));
                $images1[]=$productsClass->filename; 
                $productsClass->filename= implode(",",$images1);
                }    
            }
            $productsClass->price = strip_tags(trim($request->price1));
            $productsClass->save();
            return redirect()->route('products');
        }
        return view('admin.posts.edit', ['id'=>$productsClass->id, 'title'=>$productsClass->title, 'short_description'=>$productsClass->short_description, 'description'=>$productsClass->description, 'filename'=>$productsClass->filename, 'price'=>$productsClass->price]);
    }
    
    public function destroy($id)
    {
        $products_Class = Products::find($id);
        $products_Class->delete();
        return redirect()->route('products');
    }
}
