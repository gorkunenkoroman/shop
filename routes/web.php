<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');
Route::get('/admin/posts', 'Admin\PostsController@index')->name('products');



Route::get('/', 'Admin\PostsController@nine')->name('welcome');
Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::get('/product/{id}', 'Admin\PostsController@show')->name('product');

Route::group([
    'middleware' => 'auth'   
], function(){

Route::get('/admin', 'Admin\DashboardController@index')->name('admin');
Route::any('/admin/posts/create', 'Admin\PostsController@create')->name('create');
Route::any('/admin/posts/edit/{id}', 'Admin\PostsController@edit')->name('edit');
Route::any('/admin/posts/del/{id}', 'Admin\PostsController@destroy')->name('del');
});

Route::get('/404', function () {
    return abort(404);
});
Route::get('/500', function () {
    return abort(500);
});