@extends('layouts.product')

@section('content')
<!-- News-->
<div id="product" class="news ha-waypoint" data-animate-down="ha-header-small " data-animate-up="ha-header-large">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="imgSwitch">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 dbox-list prod-cnt graphic">
                                <div class="product-preview">
                                   <h3 class="product-title">{{$post->title}}</h3>
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-4 col-md-4 dbox-list product graphic">
                                            <div class="product-photo">
                                                <a data-fancybox="gallery" href="../uploads/{{explode(',', $post->filename)[0]}}"><img class="img-responsive" alt="product" src="../uploads/{{explode(',', $post->filename)[0]}}"></a>
                                            </div>
                                            <?php $arr = explode(',', $post->filename);?>
                                            @for ($i = 1; $i < count($arr); $i++) 
                                                <div class="thumb_img col-xs-4 col-sm-4 col-md-4 dbox-list prod-cnt graphic">
                                            <a data-fancybox="gallery" href="../uploads/{{$arr[$i]}}"><img class="img-responsive" src="../uploads/{{$arr[$i]}}"></a>
                                                </div>
                                            @endfor
                                        </div>
                                        <div class="col-xs-8 col-sm-8 col-md-8 dbox-list product2 graphic">
                                        <div class="product-price">
                                           Цена: {{$post->price}} грн
                                        </div>
                                        <p class="detail-dwp-title-i"><span class="bold">Гарантия&nbsp;</span><!----><!----><span class="ng-star-inserted">12 месяцев</span><span></span><!----> обмен/возврат товара в&nbsp;течение 14&nbsp;дней <!----></p>
                                        <p class="detail-dwp-title-i"><span class="bold">Оплата</span> Наличными, Visa/MasterCard, Приват24, Кредит, Оплата частями, Мгновенная рассрочка, Безналичными </p>
                                        <p class="product-info">
                                            <?php echo "$post->description" ?>
                                        </p>
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- news -->
@endsection
