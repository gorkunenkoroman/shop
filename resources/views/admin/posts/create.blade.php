@extends('admin.layout') @section('content')
<div class="content-wrapper">
    <form method="POST" action="{{route('create')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <section class="content-header">
            <h1>
                Добавить товар
            </h1>
        </section>

        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем товар</h3>

                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input type="text" class="form-control" id="title" placeholder="" name="title" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="InputFile">Изображения</label>
                            <input type="file" id="InputFile" name="image[]" multiple="multiple" required>
                        </div>

                        <div class="form-group">
                            <label for="price">Цена</label>
                            <input type="number" id="price" name="price" required>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <textarea name="description" id="" cols="30" rows="10" class="form-control" required>{{old('description')}}</textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="short_description">Краткое описание</label>
                        <textarea name="short_description" id="" cols="30" rows="10" class="form-control" required>{{old('short_description')}}</textarea>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input type="submit" name='enter' value='Добавить' id='btlAuth' class="btn btn-success pull-right">
            </div>
        </section>
    </form>
</div>
@endsection
