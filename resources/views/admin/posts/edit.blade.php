@extends('admin.layout') @section('content')
<div class="content-wrapper">
    <form method="POST" action="{{route('edit',$id)}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <section class="content-header">
            <h1>Изменить продукт: <small>{{$title}}</small></h1>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Изменяем продукт</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input type="text" class="form-control" id="title" placeholder="" name="title1"  value="{{$title}}" required>
                        </div>
                        <div class="form-group">
                            <label for="InputFile">Изображения</label>
                            <input type="file" id="InputFile" name="images[]" multiple="multiple" required>
                        </div>

                        <div class="form-group">
                            <label for="price1">Цена</label>
                            <input type="number" id="price1" name="price1" value="{{$price}}" required>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Описание</label>
                        <textarea name="description1" id="" cols="30" rows="10" class="form-control" required>{{$description}}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="short_description">Краткое описание</label>
                        <textarea name="short_description1" id="" cols="30" rows="10" class="form-control" required>{{$short_description}}</textarea>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input type="submit" name='edit' value='Изменить' id='btlAuth' class="btn btn-success pull-right">
            </div>
        </section>
    </form>
</div>
@endsection
