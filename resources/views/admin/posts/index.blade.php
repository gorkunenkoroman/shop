@extends('admin.layout') @section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Товары</h1>
        <ol class="breadcrumb">
            <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Главная</a></li>
        </ol>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Листинг товаров</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <a href="{{route('create')}}" class="btn btn-success">Добавить</a>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Краткое описание</th>
                            <th>Цена</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($all as $products)
                        <tr>
                            <td>{{$products->id}}</td>
                            <td>{{$products->title}}</td>
                            <td>{{$products->short_description}}</td>
                            <td>{{$products->price}}</td>
                            <td>
                                <a href="{{route('edit',$products->id)}}" class="fa fa-pencil"></a>
                                <a href="{{route('del',$products->id)}}" class="fa fa-remove"></a>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
                {{$all->links()}}
            </div>
        </div>
    </section>
</div>
@endsection
