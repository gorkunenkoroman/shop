@extends('admin.layout') @section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
Тестовое задание PHP Developer
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Главная</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">

            <div class="box-body">
                <p>Приложение должно позволять:</p>
                <p>•	Авторизоваться - модуль авторизации можно взять из уже написанных или написать самому. Создать несколько пользователей для тестирования.</p>
                <p>•	Просмотреть каталог товаров - около 20 товаров (с ценой, наименованием, несколькими фото и описанием). Товары должны вытягиваться из БД.</p>
                <p>Требования:</p>
                <p>•	Сайт должен использовать реляционную базу данных.</p>
                <p>•	Записи о товарах должны храниться в БД.</p>
                <p>Пожелания:</p>
                <p>•	Проверка вводимых пользователем значений на клиенте и сервере.</p>
                <p>•	Реализовать добавление, удаление товаров в БД.</p>
                <p>•	Разделение логики.</p>
                <p>•	Использование JavaScript и AJAX-технологии.</p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
@endsection
