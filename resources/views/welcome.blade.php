@extends('layouts.products')

@section('content')
    <div class="content">
        <div class="row">
            @foreach ($all as $products)
            <div class="col-md-4">
                <div class="product-preview">
                    <div class="product-photo">
                        <div class="product-price">
                            {{$products->price}} <sub>грн</sub>
                        </div>
                        <img alt="product" width="320px" height="240px" src="../uploads/{{explode(",", $products->filename)[0]}}">
                    </div>
                    <h3 class="product-title">{{$products->title}}</h3>
                    <p class="product-info">
                        <?php echo "$products->short_description" ?>
                    </p>
                        <a href="{{route('product', $products->id)}}"><button type="submit" class="cart-trigger button-clean button-text-small">Подробнее</button></a>
                </div>
            </div>
            @endforeach
        </div>
{{$all->links()}}
    </div>
    @endsection
    <script scr="/js/main.js"></script>
